
const Stripe = require('stripe');
const STRIPE_TEST_SECRET_KEY = 'rk_test_51EDa6GIrkMUcWfFwXon9vsJYpTqX2eTqbINAUf4fZC7ivToWv59cAPoHdYhmszwL9ZKWJtUUbaCcHtpjl6rWlXLP00C1dcAoUR'
const stripe = Stripe(STRIPE_TEST_SECRET_KEY);

const countryCodes = require('./countries-ISO3166.json');
const customers = require('./customers.json');

const {
  sortArrayFromValue,
  findObjectStringByKey,
  transformCustomers,
  pushObjectToArray,
  asyncronousArrayIteration,
  writeObjectToFile

} = require('./utilities')

// console.log(transformCustomers('france'))
const handler = async (country) => {

  try {
    let finalCustomers = []

    /* add code below this line */

    // filter the customers by country
    // get array of customers based on country
    const sortedCustomers = sortArrayFromValue(customers, country, 'country');
    // Get the countrycode from the country object
    const sortedCountryCode = findObjectStringByKey(countryCodes, country);
    // create a new array by swapping country full names to countrycodes
    const customersTransformedForStripe = transformCustomers(sortedCustomers, sortedCountryCode)

    // for each customer create a Stripe customer

    // async function that will process all customers into finalCustomers
    const createFinalCustomer = async (customer) => {
      // destructure email and country from object
      // needed to add the address line because the Stripe API does not allow an address object without it 
      const { email, country, first_name, last_name, address_line_1 } = customer;
      const name = first_name + ' ' + last_name;
      // create object to pass to stripe when creating customer
      const stripeUserParams = {
        email: email,
        name: name,
        address: {
          line1: address_line_1,
          country: country
        }
      }
      try {
        // connect to stripe and create and id 
        const createStripeCustomer = await stripe.customers.create({ ...stripeUserParams })
        // pull the id off of the returned object and alias it as customerID
        const { id: customerId } = createStripeCustomer;
        // create a new object for the final customer putting together email, customerId and country
        const transformedCustomer = { email, customerId, country }
        // push  this finalCustomer object into the finalCustomers
        const stripeToFinal = pushObjectToArray(transformedCustomer, finalCustomers)
      } catch (e) {
        throw (e)
      }
    }

    // instantiating the loop to run the prepared customers array through the createFinalCustomer function
    const processCustomersToFinal = await asyncronousArrayIteration(customersTransformedForStripe, createFinalCustomer)

    // write finalCustomers array into final-customers.json using fs
    const finalCustomersToFile = await writeObjectToFile(finalCustomers, './final-customers.json')

    //     /* add code above this line */

    console.log('finalCustomers = ')
    console.log(finalCustomers)

  } catch (e) {
    throw e
  }

}

// Run the function with Spain as the value
handler('Spain');
