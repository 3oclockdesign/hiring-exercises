// Country Utilities

// 1 ----------------------------------------------------------
// Sort throught an array and make new array ob objects containing a declared value
const sortArrayFromValue = (arrayToSort, valueToSortBy, objectKey) => {
  // create a new array with customers filtered
  const arraySortedByValue = arrayToSort.filter(
    obj => (
      // return any object where the Key is equal to the declared value
      obj[objectKey] === valueToSortBy && obj
    ));

  return (
    // return the new array 
    arraySortedByValue
  )
}

module.exports = { sortArrayFromValue };


// 2 ----------------------------------------------------------
// Get the country code of the designed country
const findObjectStringByKey = (objectToBeSearched, keyToMatch) => {
  // new array from the objectToBeSearched
  const arrayWithFilteredObject = Object.getOwnPropertyNames(objectToBeSearched).filter(
    // match the object by it's key when the value is equal to the keyToMatch and return the key
    key => (
      objectToBeSearched[key] === keyToMatch && key
    ));
  // convert the array into a string 
  const foundObjectString = arrayWithFilteredObject.toString()

  return (
    // return the country code
    foundObjectString
  )
}

module.exports = { findObjectStringByKey, ...module.exports };




// 3 ----------------------------------------------------------
// transform customers to save into Stripe
const transformCustomers = (sortedCustomers, sortedCountryCode) => {

  const customersTransformedForStripeCustomers = sortedCustomers.map(
    customer => {
      // replace country in iterated customer object
      customer.country = sortedCountryCode
      return (
        {
          ...customer // spread the modified object into the customersTransformedForStripeCustomers
        })
    }
  )
  return customersTransformedForStripeCustomers;
}


module.exports = { transformCustomers, ...module.exports };




// 4 ------------------------------------------------------------
// Push object into an array
const pushObjectToArray = (objectToPush, arrayToPushTo) => {
  arrayToPushTo.push(objectToPush)
}

module.exports = { pushObjectToArray, ...module.exports };




// 5---------------------------------------------------------------
// Write an array to file as JSON
const fs = require('fs')
const writeObjectToFile = (objectToWrite, fileLocation) => {
  fs.writeFile( //write file
    fileLocation, // to this location
    JSON.stringify(objectToWrite), // convert to string
    function (err) {
      console.log(err
        ? 'Error :' + err
        : 'Success! Written the array has been. An interview may I please haz?')
    }
  )

}

module.exports = { writeObjectToFile, ...module.exports };

// 6---------------------------------------------------------------
// a function to asyncrously iterate through an array while trasnforming each object with a given function
// will wait for the whole array to resolve
const asyncronousArrayIteration = async (arrayToIterate, functionToApplyToObject) => {
  try {
    for await (const obj of arrayToIterate) {
      await functionToApplyToObject(obj)
    }
  } catch (e) {
    throw (e)
  }

}


module.exports = { asyncronousArrayIteration, ...module.exports };