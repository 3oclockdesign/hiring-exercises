import React from 'react';
import { useQuery, gql } from '@apollo/client';
import Loading from './Loading';
import ErrorComponent from './ErrorComponent';

import UserCard from '../components/UserCard';
import UserPosts from '../components/UserPosts';

// query the data
const GET_USER = gql`
{
  user(id: 1) {
    id
    name
    address {
      street
      suite
      city
    }
    email
    phone
    company {
      name
    }
    posts(options: { paginate: { page: 1, limit: 10 } }) {
      data {
        id
        title
      }
    }
  }
}
`;


const UserProfile = () => {
  // deconstruct the receiced object
  const { loading, error, data } = useQuery(GET_USER);
  // while loading display the loading component
  if (loading) return <Loading />;
  // if we have an error then display the error component and pass the error into it
  if (error) return <ErrorComponent error={error} />;

  // assigning variable to keep the rest of the code cleaner
  const usersData = { ...data.user }
  // Spreading the data into each component
  return (
    <>
      <UserCard {...usersData} />
      <UserPosts {...usersData} />
    </>
  )
};


export default UserProfile;