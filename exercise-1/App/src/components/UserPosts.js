import React from 'react';

// The first element in the userposts
const UserPostHeading = ({ userName }) => (
  <p className="pt-4 text-base font-bold flex items-center justify-center lg:justify-start mt-4">
    {userName}'s Posts:
  </p>
)

// Component holding the text for each post title
const UserPostListing = ({ title }) => (
  <p className="pt-2 text-gray-600 text-xs lg:text-sm flex items-center justify-center lg:justify-start">
    Title: {title}
  </p>
)

// the container for the posts cards
// exported so we can use as a holder of error and loading messages
export const UserPostsBlock = ({ children }) => (
  <div className="w-full lg:w-2/5 mx-6 lg:mx-0 h-screen py-12">
    <div className="w-full rounded-lg lg:rounded-r-lg lg:rounded-l-none shadow-2xl bg-white opacity-75 mx-6 lg:mx-0 px-12 h-full overflow-auto">
      {children}
    </div>
  </div>
)

// The UserPosts component
const UserPosts = ({ posts, name }) => {
  // assigning to keep code simple
  const postList = posts.data

  return (
    <UserPostsBlock>
      <UserPostHeading userName={name} />
      {postList.map(// iterating/mapping the posts object
        post => (
          <UserPostListing key={post.id}{...post} />//spreading the post object into UserPostListing component
        )
      )}
    </UserPostsBlock>
  )
};

export default UserPosts;