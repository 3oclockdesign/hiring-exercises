import React from 'react';
import { UserCardBlock } from './UserCard';
import { UserPostsBlock } from './UserPosts';
import Emoji from './Emogi';

// A reusable block for he errors as we are loading then in both cards
const ErrorBlock = ({ error }) => (
  <>
    <div className="text-gray-900 font-bold text-4xl mb-12">Error <Emoji aria-label='Sadface' symbol='😢' /></div>
    <div className="text-gray-900 font-bold text-4xl mb-12">{error}</div>
  </>
)

const ErrorComponent = ({ error }) => {
  return (
    <>
      <UserCardBlock>
        <ErrorBlock error='error' />
      </UserCardBlock>
      <UserPostsBlock>
        <ErrorBlock error='error' />
      </UserPostsBlock>
    </>

  )
};


export default ErrorComponent;