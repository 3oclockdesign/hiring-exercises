import React from 'react';

//An array with all the classes that will be spread into the body tag
export const bodyClasses = [
  'font-sans', 'antialiased', 'text-gray-900', 'leading-normal', 'tracking-wider', 'bg-cover', 'p12']

// The path to the background image for the body tag
export const bodyBackGroundImage = 'https://source.unsplash.com/1L71sPT5XKc';


// The main container for the app
const AppWrapper = ({ children }) => (
  <div className='max-w-4xl flex items-center h-auto lg:h-screen flex-wrap mx-auto my-32 lg:my-0'>
    {children}
  </div>
)

export default AppWrapper;