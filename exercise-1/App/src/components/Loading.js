import React from 'react';
import { UserCardBlock } from './UserCard'
import { UserPostsBlock } from './UserPosts';
import Emoji from './Emogi';

// A reusable block for the loading 
const LoadingBlock = () => (
  <div className="text-gray-900 font-bold text-md mb-12"><Emoji arial-label='Hourglass' symbol='⏳' /> Loading... </div>
);

const Loading = () => {
  return (
    <>
      <UserCardBlock>
        <LoadingBlock />
      </UserCardBlock>
      <UserPostsBlock>
        <LoadingBlock />
      </UserPostsBlock>
    </>
  )
};

export default Loading;