import React from 'react';


const Emoji = ({ symbol }) => (
  <span role="img">{symbol}</span>
);

export default Emoji;