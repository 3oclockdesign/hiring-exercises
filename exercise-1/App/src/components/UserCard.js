import React from 'react';

// The topmost block of the UserCard component
const UserName = ({ name }) => (
  <>
    <h1 className='text-3xl font-bold pt-8 lg:pt-0'>
      {name}
    </h1>
    <UserRuler />
  </>
)

// A Styled presentational element to be placed inside UserName component
const UserRuler = () => (
  <div className='mx-auto lg:mx-0 w-4/5 pt-3 border-b-2 border-teal-500 opacity-25'>
  </div>
)

// UserDataKey component to go into UserData component
const UserDataKey = ({ dataKey }) => (
  <p className='pt-4 text-base font-bold flex items-center justify-center lg:justify-start'>
    {dataKey}:
  </p>
)
// UserDataValue component to go into the UserData component
const UserDataValue = ({ dataValue }) => (
  <p className='pt-2 text-gray-600 text-xs lg:text-sm flex items-center justify-center lg:justify-start'>
    {dataValue}
  </p>
)

// UserData component 
// get the key and value and displays each one styled 
// ie: Address: 21 Baker St, London
const UserData = ({ dataKey, dataValue }) => (
  < >
    <UserDataKey dataKey={dataKey} />
    <UserDataValue dataValue={dataValue} />
  </>
)
// The container for the UserCard
// exported so we can use as a holder of error and loading messages
export const UserCardBlock = ({ children }) => (
  <div id='profile' className='w-full lg:w-3/5 rounded-lg lg:rounded-l-lg lg:rounded-r-none shadow-2xl bg-white opacity-75 mx-6 lg:mx-0'>
    <div className='p-4 md:p-12 text-center lg:text-left'>
      {children}
    </div>
  </div>
)


const UserCard = ({ name, email, phone, company, address }) => { //destructuring the props 
  const { name: companyName } = company; //destructure and alias so as to not clash with prop with the same name
  const { street, suite, city } = address; // destructure the address
  const addressString = `${street} ${suite} ${city}`;// put the address parts into a template literal

  return (
    <UserCardBlock>
      <UserName name={name} />
      <UserData dataKey={'Address'} dataValue={addressString} />
      <UserData dataKey={'Email'} dataValue={email} />
      <UserData dataKey={'Phone'} dataValue={phone} />
      <UserData dataKey={'Company'} dataValue={companyName} />
    </UserCardBlock>

  )
};

export default UserCard;