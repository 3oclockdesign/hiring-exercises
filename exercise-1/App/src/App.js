import React from 'react';
import UserProfile from './components/UserProfile'
import { ApolloProvider } from '@apollo/client';
import { ApolloClient, InMemoryCache } from '@apollo/client';

import AppWrapper, { bodyClasses, bodyBackGroundImage } from './components/PageLayout';



const client = new ApolloClient({
  uri: 'https://graphqlzero.almansi.me/api',
  cache: new InMemoryCache()
});


// target the body tag
// spread the bodyClasses array  into it
document.body.classList.add(...bodyClasses);
//target the body tag  and add the style backgroun image by way of a template litteral
document.body.style.backgroundImage = `url(${bodyBackGroundImage})`;


function App() {
  return (
    <AppWrapper>
      <ApolloProvider client={client}>
        <UserProfile />
      </ApolloProvider>
    </AppWrapper>

  );
}

export default App;